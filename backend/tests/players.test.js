import Players from '../src/modules/players';
import * as drawMap from '../src/modules/map/drawMap';
import testUtils from './testUtils';

describe('Test Players module containing players data', () => {
  test('New players', () => {
    const originalError = console.error;
    console.error = jest.fn();
    const players = new Players;
    expect(() => {
      players.getPlayer(testUtils.test1)
    }).toThrow(new Error(`No player named ${testUtils.test1}`));
    console.error = originalError;
  });

  test('New players added', () => {
    const players = new Players;
    players.addPlayer(testUtils.test1);
    expect(players.getPlayer(testUtils.test1)).
      toStrictEqual({
        score: 0,
        travel: testUtils.emptyTravel
      });
  });

  test('New players increment score only for certain player', () => {
    const players = new Players;
    players.addPlayer(testUtils.test1);
    players.addPlayer(testUtils.test2);
    players.movePlayer(testUtils.test1, []);
    expect(players.getPlayer(testUtils.test1)).
      toStrictEqual({
        score: 1,
        travel: testUtils.emptyTravel
      });
    expect(players.getPlayer(testUtils.test2)).
      toStrictEqual({
        score: 0,
        travel: testUtils.emptyTravel
      });
    [...Array(8)].forEach(() => players.movePlayer(testUtils.test1, []));
    expect(players.getPlayer(testUtils.test1)).
      toStrictEqual({
        score: 9,
        travel: testUtils.emptyTravel
      });
  });

  test('New players move through all tiles', () => {
    const mockMap = jest.spyOn(drawMap, 'default');
    mockMap.mockReturnValue(testUtils.testMap);
    const players = new Players;
    players.addPlayer(testUtils.test1);
    mockMap.mockRestore();
    players.addPlayer(testUtils.test2);
    expect(players.getPlayer(testUtils.test1)).
      toStrictEqual({
        score: 0,
        travel: testUtils.emptyTravel
      });
    players.movePlayer(testUtils.test1, [...Array(25).keys()]);
    expect(players.getPlayer(testUtils.test1)).
      toStrictEqual({
        score: 1,
        travel: testUtils.testMap
      });
    players.movePlayer(testUtils.test2, [9]);
    expect(players.getPlayer(testUtils.test2).travel[9]).not.toBe(' ');
  });

  test('Check win and remove a player', () => {
    const mockMap = jest.spyOn(drawMap, 'default');
    mockMap.mockReturnValue(testUtils.testMap);
    const players = new Players;
    players.addPlayer(testUtils.test1);
    mockMap.mockRestore();

    let player = players.movePlayer(testUtils.test1, [1, 2, 3]);
    expect(players.removeWon(testUtils.test1)).toBeFalsy();
    expect(player).not.toBeFalsy();

    player = players.movePlayer(testUtils.test1, testUtils.testWin);
    expect(players.removeWon(testUtils.test1)).toBeTruthy();

    const originalError = console.error;
    console.error = jest.fn();
    expect(() => {
      players.getPlayer(testUtils.test1)
    }).toThrow(new Error(`No player named ${testUtils.test1}`));
    console.error = originalError;
  });

  test('Move player function returns a player +1 score if he exists or an error if he doesn\'t', () => {
    const players = new Players;
    players.addPlayer(testUtils.test1);
    expect(players.movePlayer(testUtils.test1, [])).toStrictEqual({
      score: 1,
      travel: testUtils.emptyTravel
    });
    const originalError = console.error;
    console.error = jest.fn();
    expect(() => {
      players.movePlayer(testUtils.test2)
    }).toThrow(new Error(`No player named ${testUtils.test2}`));
    console.error = originalError;
  });
});