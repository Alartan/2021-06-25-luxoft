import { expect, test } from '@jest/globals';
// neccesary for spyOn to work
import checkWon from "../../src/modules/player/checkWon";
import testUtils from '../testUtils';

describe('Test checkWon player if it is correct', () => {
  test('Open map is won', () => {
    expect(checkWon(testUtils.testMap)).toBeTruthy();
  });
  test('Empty map isn\t won', () => {
    expect(checkWon(testUtils.emptyTravel)).toBeFalsy();
  });
  test('Test map with 3 first tiles isn\'t won', () => {
    let travel = [...testUtils.emptyTravel];
    [0, 1, 2].forEach((element) => {
      travel[element] = testUtils.testMap[element];
    });
    expect(checkWon(travel)).toBeFalsy();
  })
  test('Test map with open treasures should be won', () => {
    let travel = [...testUtils.emptyTravel];
    testUtils.testWonTiles.forEach((element) => {
      travel[element] = testUtils.testMap[element];
    });
    expect(checkWon(travel)).toBeTruthy();
  })
});