import drawMap from './map/drawMap.js';
import checkWon from './player/checkWon.js';

export default class Player {
  #score;
  #map;
  #travel;

  constructor(name) {
    this.name = name;
    this.#score = 0;
    this.#map = drawMap();
    this.#travel = Array(25).fill(' ');
  }

  checkWon() {
    return checkWon(this.#travel);
  }

  incrementScore() {
    this.#score++;
  }

  showTile(tile) {
    this.#travel[tile] = this.#map[tile];
  }

  getMe() {
    return {
      score: this.#score,
      travel: this.#travel
    }
  }
}