export default function checkWon(travel) {
  return travel.reduce((sum, e) => {
    if (e === 'T') sum++;
    return sum;
  }, 0) === 3;
}