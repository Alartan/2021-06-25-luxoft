import Player from "./player.js";

export default class Players {
  #players;

  constructor() {
    this.#players = {};
  }

  addPlayer = (name) => {
    this.#players[name] = new Player(name);
  }

  getPlayer(name) {
    const player = this.#players[name];
    if (player !== undefined)
      return player.getMe();
    throw new Error(`No player named ${name}`);
  }

  removeWon(name) {
    if (this.#players[name].checkWon()){
      delete this.#players[name];
      return true;
    } 
    return false;
  }

  movePlayer(name, moves) {
    const player = this.#players[name];
    if (player !== undefined) {
      moves.forEach(element => {
        player.showTile(element);
      });
      player.incrementScore();
      return player.getMe();
    }
    else
      throw new Error(`No player named ${name}`);
  }
}